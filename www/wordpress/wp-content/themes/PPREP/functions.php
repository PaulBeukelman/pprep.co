<?php

function theme_setup() {
	add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'theme_setup' );

/* Style */
function add_style() {
	wp_register_style( 'lato', 'https://fonts.googleapis.com/css?family=Lato:300', '', '', 'screen, projection' );
	wp_enqueue_style( 'style' );
	wp_register_style( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css', '', '', 'screen, projection' );
	wp_enqueue_style( 'bootstrap' );
	wp_register_style( 'style', get_stylesheet_directory_uri() . '/style.css', '', '', 'screen, projection' );
	wp_enqueue_style( 'style' );

}
add_action( 'wp_enqueue_scripts', 'add_style' );

/* Custom Javascript */
function add_custom_javascripts() {

		wp_deregister_script( 'jquery' );

		wp_register_script( 'jquery', 'https://code.jquery.com/jquery-2.1.1.min.js' );
		wp_enqueue_script( 'jquery' );

		wp_register_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js','','1.0', true );
		wp_enqueue_script( 'bootstrap' );

		wp_register_script( 'custom', get_template_directory_uri() . '/js/custom.js','','1.0', true );
		wp_enqueue_script( 'custom' );

}
add_action( 'wp_enqueue_scripts', 'add_custom_javascripts' );


/* --------------------------------------------------------------
	Menu's
--------------------------------------------------------------*/

function register_main_menu() {
	register_nav_menu('primary-menu',__( 'Primary Menu' ));
	register_nav_menu('top-menu',__( 'top Menu' ));
}
add_action( 'init', 'register_main_menu' );


/* --------------------------------------------------------------
	Extra post type
-------------------------------------------------------------- 
function create_post_type() {
  register_post_type( 'testimonials',
    array(
      'labels' => array(
        'name' => __( 'Testimonials' ),
        'singular_name' => __( 'testimonial' )
      ),
      'public' => true,
      'has_archive' => true,
	  'rewrite' => array('slug' => 'testimonial'),
    )
  );
}
add_action( 'init', 'create_post_type' );*/
/* --------------------------------------------------------------
	Widgets
-------------------------------------------------------------- 
function bootstrap_widgets_init() {	
	register_sidebar( array(
	'name' => 'Footer Sidebar 1',
	'id' => 'footer-sidebar-1',
	'description' => 'Appears in the footer area',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
	) );
}
add_action( 'widgets_init', 'bootstrap_widgets_init' );
*/


?>