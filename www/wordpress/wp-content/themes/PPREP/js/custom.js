function changeSrc() {
    var target =    $('.lazy-hero');
    var source =    target.data('source');
    target.attr('src', source);
}

$(document).ready(function(){
    $('.nav-container a').click(function() {
        $('.nav-container a.active').removeClass('active');
        $(this).addClass('active');
    });
    if ($(window).width() > 760 ) {
        changeSrc();
    }
    $('.client-menu-item').click(function(){
        $('.single-client').removeClass('active');
        $('.client-menu-item.active').removeClass('active');
        $(this).addClass('active');
        var thumb = $(this).attr('id');
        var target = thumb.replace('-menu', '');
        $('#' + target + '-content').addClass('active');
    });
    $(function() {
        $('.scroll-to').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
    $('.indicator').click(function() {
        $('.scroll-to').parent().removeClass("active");
        $(this).parent().addClass("active");
    });
});

$(window).scroll(function() {
    var windowTop = $(window).scrollTop();
    if (windowTop > 5) {
        $('.to-top').removeClass('hidden');
    } else {
        $('.to-top').addClass('hidden');
    }
    //borders
    var offSetHead = $(window).height()*.32;
    var head1 = $('.special-header-block').offset().top - offSetHead ;
    var head2 = $('.heading-section.yellow').offset().top - offSetHead - 40;
    var head3 = $('.heading-section.green').offset().top - offSetHead - 65;
    if ( (head1 < windowTop) && (windowTop < head2) ) {
        $('#client-scroll').addClass("active");
    } else {
        $('#client-scroll, #contact').removeClass("active");
    }
    if ((head2 < windowTop) && (windowTop < head3)) {
        $('#active-scroll').addClass("active");
    } else {
        $('#active-scroll, #contact').removeClass("active");
    }
    if (windowTop > head3) {
        $('#pprep-scroll').addClass("active");
    } else {
        $('#pprep-scroll, #contact').removeClass("active");
    }

    //positions
    var menu1 = $('#client-scroll');
    var menu2 = $('#active-scroll');
    var menu3 = $('#pprep-scroll');
    var menu4 = $(' #contact');
    var offSetCor = 5;
    var menu1Off = menu1.offset().top + offSetCor;
    var menu2Off = menu2.offset().top + offSetCor;
    var menu3Off = menu3.offset().top + offSetCor;
    var menu4Off = menu4.offset().top + offSetCor;
    var headClean1 = $('.special-header-block').offset().top;
    var headClean2 = $('.heading-section.yellow').offset().top;
    var headClean3 = $('.heading-section.green').offset().top;
    var heightSection = 245;
    //head1
    if ( (headClean1 < menu1Off) && (menu1Off < (headClean1+heightSection)) ) {
        menu1.addClass('section-1-white');
    } else {
        menu1.removeClass('section-1-white');
    }
    if ( (headClean1 < menu2Off) && (menu2Off < (headClean1+heightSection)) ) {
        menu2.addClass('section-1-white');
    } else {
        menu2.removeClass('section-1-white');
    }
    if ( (headClean1 < menu3Off) && (menu3Off < (headClean1+heightSection)) ) {
        menu3.addClass('section-1-white');
    } else {
        menu3.removeClass('section-1-white');
    }
    if ( (headClean1 < menu4Off) && (menu4Off < (headClean1+heightSection)) ) {
        menu4.addClass('section-1-white');
    } else {
        menu4.removeClass('section-1-white');
    }
    //head2
    if ( (headClean2 < menu1Off) && (menu1Off < (headClean2+heightSection)) ) {
        menu1.addClass('section-2-white');
    } else {
        menu1.removeClass('section-2-white');
    }
    if ( (headClean2 < menu2Off) && (menu2Off < (headClean2+heightSection)) ) {
        menu2.addClass('section-2-white');
    } else {
        menu2.removeClass('section-2-white');
    }
    if ( (headClean2 < menu3Off) && (menu3Off < (headClean2+heightSection)) ) {
        menu3.addClass('section-2-white');
    } else {
        menu3.removeClass('section-2-white');
    }
    if ( (headClean2 < menu4Off) && (menu4Off < (headClean2+heightSection)) ) {
        menu4.addClass('section-2-white');
    } else {
        menu4.removeClass('section-2-white');
    }
    //head3
    if ( (headClean3 < menu1Off) && (menu1Off < (headClean3+heightSection)) ) {
        menu1.addClass('section-3-white');
    } else {
        menu1.removeClass('section-3-white');
    }
    if ( (headClean3 < menu2Off) && (menu2Off < (headClean3+heightSection)) ) {
        menu2.addClass('section-3-white');
    } else {
        menu2.removeClass('section-3-white');
    }
    if ( (headClean3 < menu3Off) && (menu3Off < (headClean3+heightSection)) ) {
        menu3.addClass('section-3-white');
    } else {
        menu3.removeClass('section-3-white');
    }
    if ( (headClean3 < menu4Off) && (menu4Off < (headClean3+heightSection)) ) {
        menu4.addClass('section-3-white');
    } else {
        menu4.removeClass('section-3-white');
    }


});
$(window).resize(function() {
    if ($(window).width() > 760 ) {
        changeSrc();
    }
});
$(window).load(function() {
    $('body').addClass('loaded');
});
$(window).bind('hashchange', function() {

});




