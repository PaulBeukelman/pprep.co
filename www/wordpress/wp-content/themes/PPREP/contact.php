<?php
/*
Template Name: Contact
*/

 get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); 
	$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>
		
<section class="container hero">
	<div class="row">
			<div class="hero-header">
				<?php the_field('google_maps'); ?>
			</div>
	</div><!--/row-->
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php the_content(); ?>
		</article>
	</div><!--/row-->
	
	
</section>
<?php endwhile; ?>
<?php get_footer(); ?>