<?php
 get_header(); ?>
<?php if ( have_posts() ) while ( have_posts() ) : the_post(); 
	$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>
<section class="content">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<?php echo the_content(); ?>
		</div>
	</div>
</div>
</section>
	

<?php endwhile; ?>
<?php get_footer(); ?>