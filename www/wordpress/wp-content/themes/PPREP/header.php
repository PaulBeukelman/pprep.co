<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<title><?php bloginfo( 'name' ); ?> — <?php bloginfo( 'description' ); ?></title>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php wp_head(); ?>
<!-- Icons -->
<link rel="shortcut icon" id="favicon" href="<?php echo get_template_directory_uri(); ?>/images/favicon/favicon.ico">
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-icon.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri(); ?>/images/favicon/apple-icon-152x152.png">

</head>
<body id="top" <?php body_class(); ?>>
<div class="container hero-container">
    <div class="col-sm-10 col-sm-offset-2 hero-col">
        <div class="hero-heading opening-head">
            <span class="koyu">PPREP</span><span class="gothic"> FOR WEB</span>
        </div>
        <div class="hero-heading opening-sub">
            <span class="koyu">PPREP.co</span>
        </div>
        <div class="hero-heading opening-sub-sub gothic">
            <span class="mobile-hide">Web</span>concept - <span class="mobile-hide">Web</span>design - <span class="mobile-hide">Web</span>development<span class="mobile-hide"</span>
        </div>
    </div>
    <div class="special-header-block col-sm-2"></div>
</div>
<header class="main-head">
    <div class="nav-container">
        <ul>
            <li id="client-scroll"><a class="indicator scroll-to" href="#clients">Opdrachtgevers</a></li>
            <li id="active-scroll"><a class="indicator scroll-to" href="#active-contracts">In Progress</a></li>
            <li id="pprep-scroll"><a class="indicator scroll-to" href="#pprep">Over PPREP</a></li>
            <li id="contact"><a class="indicator scroll-to" href="#contact-bottom">Contact</a></li>
        </ul>
    </div>
</header>
<a class="scroll-to to-top hidden" href="#top"><img src="<?php echo get_template_directory_uri(); ?>/images/pijl.svg"/></a>
<div class="fixed-lines">
    <div class="container">

        <div class="menu-head">
            <span>MENU</span>
        </div>

    </div>
</div>

<div class="triangle-open">
    <img class="triangle-img lazy-hero" data-source="<?php echo get_template_directory_uri(); ?>/images/header.png"" src="<?php echo get_template_directory_uri(); ?>/images/pixel.svg"/>
    <img class="r-icon" src="<?php echo get_template_directory_uri(); ?>/images/r-icon.svg"/>
</div>
<div class="special-header-red">

</div>
