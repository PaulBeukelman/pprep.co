<?php
/*
Template Name: Homepage
*/

 get_header(); ?>
<div class="content-holder">
    <div id="clients" class="container">
        <div class="row">
            <div class="col-sm-8 col-lg-4 col-lg-offset-2 heading-box">
                <h2 class="heading">Opdrachtgevers</h2>
                <div class="intro-section">
                    <p>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-lg-7 col-lg-offset-2 clients">
                <div id="sanitairwinkel-content" class="single-client active">
                    <div class="row heading-row">
                        <div class="col-sm-3">
                            <img class="client-photo" src="<?php echo get_template_directory_uri(); ?>/images/rob.jpg"/>
                        </div>
                        <div class="col-sm-9 info-row">
                            <a class="client-link" target="_blank"  href="http://sanitairwinkel.nl"><h2 class="client-title">Sanitairwinkel</h2></a>
                            <h2 class="client-quote">"We zijn superblij met het nieuwe design"</h2>
                            <h2 class="client-person">Rob van Laarhoven | CEO</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 client-content">
                            <h3 class="content-heading">Opdrachtgever</h3>
                            <p>Sanitairwinkel.nl is de grootste online sanitairwinkel van Nederland. Ze groeien ieder jaar weer en hebben daarom ook ieder jaar een gazelle notering bij het Financieel dagblad. Daar hoort een 'baas' van een frontend bij.
                            </p>
                        </div>
                        <div class="col-sm-6 client-content client-logo-col">
                            <img class="client-logo" src="<?php echo get_template_directory_uri(); ?>/images/sanitairwinkel.png"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 client-content">
                            <h3 class="content-heading">Design</h3>
                            <p>Een goed converterende webshop is niet zomaar vernieuwd. Een perfecte User Interface en Experience is daarom belangrijk. "Het moet aanvoelen als een warm bad."
                            </p>
                        </div>
                        <div class="col-sm-6 client-content">
                            <h3 class="content-heading">Development</h3>
                            <p>Effici&#235;nte code is bij veel bezoekers belangrijk. Een snelle website betekent niet alleen betere conversie, maar ook voor SEO-doeleinden is dit positief.
                            </p>
                        </div>
                    </div>
                </div>
                <div id="voradius-content" class="single-client">
                    <div class="row heading-row">
                        <div class="col-sm-3">
                            <img class="client-photo" src="<?php echo get_template_directory_uri(); ?>/images/sebastiaan_ambtman.png"/>
                        </div>
                        <div class="col-sm-9 info-row">
                            <a class="client-link" target="_blank"  href="http://voradius.nl"><h2 class="client-title">Voradius.nl</h2></a>
                            <h2 class="client-quote">"Samen met PPREP maken we mooie nieuwe front-ends voor onze producten"</h2>
                            <h2 class="client-person">Sebastiaan Ambtman | Productowner</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 client-content">
                            <h3 class="content-heading">Voradius</h3>
                            <p>Voradius bouwt aan een systeem dat real time inzicht biedt in voorraad van offline winkels. Met Voradius kun je dus in een oogopslag zien of een product te koop is bij jou in de buurt.
                            </p>
                        </div>
                        <div class="col-sm-6 client-content client-logo-col">
                            <img class="client-logo" src="<?php echo get_template_directory_uri(); ?>/images/voradius-logo.png"/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 client-content">
                            <h3 class="content-heading">Frontend</h3>
                            <p>De frontend is full responsive en gebouwd met bootstrap, backend framework dat hier is gebruikt is Yii 2.0. De uitdaging in deze opdracht zat hem vooral in snelheid, zowel performance van de website als flexibel en snel tot een of meerdere producten kunnen komen, zodat er getest kan worden.
                            </p>
                        </div>
                        <div class="col-sm-6 client-content">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 client-images">
                            <img class="client-logo" src="<?php echo get_template_directory_uri(); ?>/images/voradius1.png"/>
                        </div>
                        <div class="col-sm-4 client-images">
                            <img class="client-logo" src="<?php echo get_template_directory_uri(); ?>/images/voradius2.png"/>
                        </div>
                        <div class="col-sm-4 client-images">
                            <img class="client-logo" src="<?php echo get_template_directory_uri(); ?>/images/voradius3.png"/>
                        </div>
                    </div>
                </div>
                <div id="efectos-content" class="single-client">
                    <div class="row heading-row">
                        <div class="col-sm-3">
                            <img class="client-photo" src="<?php echo get_template_directory_uri(); ?>/images/stefan.jpg"/>
                        </div>
                        <div class="col-sm-9 info-row">
                            <a class="client-link" target="_blank"  href="http://eFectos .nl"><h2 class="client-title">eFectos.nl</h2></a>
                            <h2 class="client-quote">"PPREP is flexibel, is altijd bereid mee te denken en de communicatie verloopt altijd helder."</h2>
                            <h2 class="client-person">Stefan Doorn | Senior Back-end developer</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 client-content">
                            <h3 class="content-heading">Efectos</h3>
                            <p>EFectos is Stefan Doorn een senior back-end developer werkzaam als freelancer voor verschillende grote websites. Bij elk back-end project hoort een frontend, PPREP helpt om deze te realiseren.
                            </p>
                        </div>
                        <div class="col-sm-6 client-content client-logo-col">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 client-content">
                            <h3 class="content-heading">Efectos over ons</h3>
                            <p>De laatste jaren heb ik regelmatig met PPREP samengewerkt. Als backend developer ben ik regelmatig betrokken bij projecten waar ook design & front-end werkzaamheden nodig zijn. Ik werk dan graag samen met PPREP.
                            </p>
                        </div>
                        <div class="col-sm-6 client-content">
                            <h3 class="content-heading">&nbsp;</h3>
                            <p>Het is prettig om de front-end bij &#233;&#233;n partij neer te kunnen leggen, met de zekerheid dat er kwaliteit geleverd wordt. Inmiddels heb ik bij meerdere projecten met PPREP samengewerkt, tot volle tevredenheid.</p>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-sm-3 col-sm-offset-1 col-lg-2 menu-clients">
                <div class="row">
                    <div id="sanitairwinkel-menu"  class="client-menu-item active">
                        <div class="col-sm-4"><img class="client-menu-image" src="<?php echo get_template_directory_uri(); ?>/images/rob.jpg"/></div>
                        <div class="col-sm-8"><h2 class="client-menu-title">sanitairwinkel.nl</h2></div>
                    </div>
                    <div id="voradius-menu" class="client-menu-item">
                        <div class="col-sm-4"><img class="client-menu-image" src="<?php echo get_template_directory_uri(); ?>/images/sebastiaan_ambtman.png"/></div>
                        <div class="col-sm-8"><h2 class="client-menu-title">Voradius</h2></div>
                    </div>
                    <div id="efectos-menu" class="client-menu-item">
                        <div class="col-sm-4"><img class="client-menu-image" src="<?php echo get_template_directory_uri(); ?>/images/stefan.jpg"/></div>
                        <div class="col-sm-8"><h2 class="client-menu-title">eFectos</h2></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="heading-section yellow">
        <div id="active-contracts" class="container">
            <div class="row">
                <div class="col-sm-8 col-lg-4 col-lg-offset-2 heading-box">
                    <h2 class="heading">In progress</h2>
                    <div class="intro-section">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="after-heading-section">
        <div class="container">
            <div class="row active-contracts">
                <div class="col-sm-8 col-lg-7 col-lg-offset-2">
                    <div class="row">
                        <div class="col-sm-6">
                            <img class="active-contract-image" src="<?php echo get_template_directory_uri(); ?>/images/Lopendeopdrachten-01.png"/>
                        </div>
                        <div class="col-sm-6">
                            <img class="active-contract-image" src="<?php echo get_template_directory_uri(); ?>/images//Lopendeopdrachten-02.png"/>
                        </div>
                        <div class="col-sm-6">
                            <img class="active-contract-image" src="<?php echo get_template_directory_uri(); ?>/images//Lopendeopdrachten-04.png"/>
                        </div>
                         <div class="col-sm-6">
                            <img class="active-contract-image" src="<?php echo get_template_directory_uri(); ?>/images//lopendeopdrachten-05.png"/>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-3">
                    <h3 class="content-heading company-info">Wat zijn we aan het doen?</h3>
                    <p>Op dit moment worden schetsen uitgewerkt, code geschreven, prototypen gepresenteerd en Frontends opgeleverd.
                    Huh!? Maken jullie ook dingen voor Halo en FIFA?. Nee, helaas, we spelen wel graag hun games. Dat dan weer wel...
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="heading-section green">
        <div id="pprep" class="container">
            <div class="row">
                <div class="col-sm-8 col-lg-4 col-lg-offset-2 heading-box">
                    <h2 class="heading">Over PPREP</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="after-heading-section last-section">
        <div class="container">
            <div class="row active-contracts">
                <div class="col-sm-8 col-lg-7 col-lg-offset-2">
                    <div class="row">
                        <div class="col-sm-6">
                            <img class="active-contract-image add-shade" src="<?php echo get_template_directory_uri(); ?>/images/evert.jpg"/>
                            <h4>Evert Jan de Vries</h4>
                            <h5><em>Visual designer | UI | UE</em></h5>
                            <p>

                            </p>
                        </div>
                        <div class="col-sm-6">
                            <img class="active-contract-image add-shade" src="<?php echo get_template_directory_uri(); ?>/images/paul.jpg"/>
                            <h4>Paul Beukelman</h4>
                            <h5><em>Front-end developer</em></h5>
                            <p>

                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-3">
                    <h3 class="content-heading company-info">PPREP.co</h3>
                    <p>Wij maken websites en zoeken daarbij de randen van de comfortzone op. Out of the box en strevend naar perfectie. PPREP.co: Concept - Design - Development.
                        You ready to be PPREPPED?
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div id="contact-bottom" class="triangle-overflow">
        <div class="container">
            <div class="row copy-foot">
                <div class="col-xs-12 col-sm-8 col-lg-7 col-lg-offset-2">
                    <h3 class="foot-head">Contact</h3>
                    <div class="row">
                        <div class="col-sm-6">
                            <p>
                                Evert-Jan:<br/>
                                <a href="mailto:evert-jan@pprep.co">evert-jan@pprep.co</a>
                            </p>
                        </div>
                        <div class="col-sm-6">
                            <p>
                                Paul:<br/>
                                <a href="mailto:paul@pprep.co">paul@pprep.co</a><br/>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="triangle-foot">
            <div class="top-layer-triangle">

            </div>
            <div class="footer-line">
                <span class="koyu">PPREP</span><span class="gothic"> FOR WEB</span>
            </div>
            <div class="footer-line two">
                <span class="koyu">PPREP.co</span>
            </div>
        </div>
    </div>
</div>

<?php if ( have_posts() ) while ( have_posts() ) : the_post();
	$feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
?>


<?php endwhile; ?>
<?php get_footer(); ?>
